#ifndef TRACK_AMBI_DECORATOR_HH
#define TRACK_AMBI_DECORATOR_HH

#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticleContainer.h"

#include <string>

// this is a bare-bones example of a class that could be used to
// decorate a track with additional information

class TrackAmbiDecorator
{
public:
  TrackAmbiDecorator(const std::string& decorator_prefix = "");

  // this is the function that actually does the decoration
  void decorateAll(const xAOD::TrackParticleContainer& tracks) const;
private:

  SG::AuxElement::Decorator<float> m_ambiRank;
};

#endif
