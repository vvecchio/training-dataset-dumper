#ifndef SINGLE_BTAG_TOOLS_HH
#define SINGLE_BTAG_TOOLS_HH

#include <optional>


#include "BTagJetWriter.hh"
#include "BTagTrackWriter.hh"
#include "TrackSelector.hh"
#include "trackSort.hh"
#include "TruthWriter.hh"
#include "DecoratorExample.hh"
#include "JetTruthDecorator.hh"
#include "TrackTruthDecorator.hh"
#include "TrackVertexDecorator.hh"
#include "TrackLeptonDecorator.hh"
#include "TrackAmbiDecorator.hh"
#include "TruthCorruptionCounter.hh"
#include "HitDecorator.hh"
#include "BJetShallowCopier.hh"
#include "HitWriter.hh"
#include "JetTruthDecorator.hh"
#include "TruthSelectorConfig.hh"
#include "JetLeptonDecayLabelDecorator.hh"

#include "FlavorTagDiscriminants/BTagJetAugmenter.h"
#include "FlavorTagDiscriminants/BTagTrackIpAccessor.h"
#include "FlavorTagDiscriminants/BTagMuonAugmenter.h"
#include "FlavorTagDiscriminants/DL2HighLevel.h"

#include "JetCalibTools/JetCalibrationTool.h"
#include "JetSelectorTools/JetCleaningTool.h"
#include "JetMomentTools/JetVertexTaggerTool.h"
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"

struct SingleBTagConfig;
struct TrackConfig;
struct TruthConfig;

namespace H5 {
  class H5File;
}

struct TrackToolkit {
  using AE = SG::AuxElement;
  TrackToolkit(const TrackConfig&, H5::Group&);
  TrackToolkit(TrackToolkit&&);
  TrackSelector selector;
  TrackSort sort;
  BTagTrackWriter writer;
  BTagTrackIpAccessor track_accessor;
  SG::AuxElement::Decorator<int> n_tracks_decorator;
};

struct SingleBTagTools {

  using AE = SG::AuxElement;

  struct Accessors {
    Accessors(const SingleBTagConfig& cfg);
    AE::ConstAccessor<char> eventClean_looseBad;
    AE::ConstAccessor<ElementLink<xAOD::BTaggingContainer>> btaggingLink;
    AE::ConstAccessor<float> jvt;
  };

  struct Decorators {
    AE::Decorator<float> jvt;
    AE::Decorator<float> fcal_et_tev;
    AE::Decorator<int> jet_rank;
    AE::Decorator<float> trackjet_rel_dR;  // PG: temporary fix for VR track jet decorators missing
    AE::Decorator<float> trackjet_abs_dR;  // PG: temporary fix for VR track jet decorators missing
    AE::Decorator<int> n_primary_vertices;
    AE::Decorator<float> primary_vertex_detector_z;
    AE::Decorator<float> primary_vertex_detector_z_uncertainty;
    Decorators():
      jvt("bTagJVT"),
      fcal_et_tev("FCal_Et_TeV"),
      jet_rank("jetPtRank"),
      trackjet_rel_dR("relativeDeltaRToVRJet"), // PG: temporary fix for VR track jet decorators missing
      trackjet_abs_dR("deltaRToVRJet"),         // PG: temporary fix for VR track jet decorators missing
      n_primary_vertices("nPrimaryVertices"),
      primary_vertex_detector_z("primaryVertexDetectorZ"),
      primary_vertex_detector_z_uncertainty(
        "primaryVertexDetectorZUncertainty")
      {}
  };

  SingleBTagTools(const SingleBTagConfig&);
  JetCalibrationTool calibration_tool;
  JetCleaningTool cleaning_tool;
  JetVertexTaggerTool jvttool;

  BJetShallowCopier shallow_copier;
  BTagJetAugmenter jet_augmenter;
  FlavorTagDiscriminants::BTagMuonAugmenter muon_augmenter;

  std::vector<FlavorTagDiscriminants::DL2HighLevel> dl2s;

  Accessors acc;
  Decorators dec;

  std::optional<JetTruthDecorator> hadrons;
  std::optional<JetTruthDecorator> leptons;
  std::optional<JetTruthDecorator> frombc;

  TrackTruthDecorator trkTruthDecorator;
  TrackVertexDecorator trkVertexDecorator;
  TrackLeptonDecorator trkLeptonDecorator;
  TrackAmbiDecorator trkAmbiDecorator;
  DecoratorExample example_decorator;
  JetLeptonDecayLabelDecorator jet_lepton_decay_label_decorator;


  std::unique_ptr<HitDecorator> hit_decorator;

};

struct SingleBTagOutputs {
  SingleBTagOutputs(const SingleBTagConfig&, H5::Group&);

  BTagJetWriter jet_writer;
  std::vector<TrackToolkit> tracks;
  std::vector<TruthWriter> truths;
  std::unique_ptr<HitWriter> hits;

  TruthCorruptionCounter truth_counts;

};

#endif
