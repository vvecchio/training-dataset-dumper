#include "HitDecorator.hh"
#include "HitDecoratorConfig.hh"

#include "GeoPrimitives/GeoPrimitives.h"
#include "xAODJet/Jet.h"
#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "xAODTracking/Vertex.h"

namespace {

  // couple functions to get 3 vectors from various places
  Amg::Vector3D get3Vector(const xAOD::TrackMeasurementValidation& h) {
    return {h.globalX(), h.globalY(), h.globalZ()};
  }

  Amg::Vector3D getShifted3Vector(
    const xAOD::TrackMeasurementValidation& h,
    const xAOD::Vertex& v) {
    Amg::Vector3D p = v.position();
    return get3Vector(h) - p;
  }

}


// you have to give the decorators their names here
HitDecorator::HitDecorator(const HitDecoratorConfig& cfg):
  m_nHits_L0("nHits_L0"),
  m_nHits_L1("nHits_L1"),
  m_nHits_L2("nHits_L2"),
  m_nHits_L3("nHits_L3"),
  m_dR_hit_to_jet(cfg.dR_hit_to_jet),
  m_save_endcap_hits(cfg.save_endcap_hits),
  m_layer(Acc<int>("layer")),
  m_bec(Acc<int>("bec"))
{
}

// the main function you'll call
void HitDecorator::decorate(
  const xAOD::Jet& jet,
  const xAOD::TrackMeasurementValidationContainer& hits,
  const xAOD::Vertex& vertex) const
{

  // loop over all the hits and save the ones that are within some
  // radius
  int nHitsL0(0), nHitsL1(0), nHitsL2(0), nHitsL3(0);
  for (const auto* raw_hit: hits) {
    // Do not count the hit if it's in EC and we only want barrel hits
    int isEC = (abs(m_bec(*raw_hit)) == 2);
    if (isEC && !m_save_endcap_hits) continue;
    
    // Count hits within a given dR
    TVector3 local(getShifted3Vector(*raw_hit, vertex).data());
    if (jet.p4().Vect().DeltaR(local) < m_dR_hit_to_jet) {  
      int layer = m_layer(*raw_hit);
      if (layer == 0) ++nHitsL0;
      else if (layer == 1) ++nHitsL1;
      else if (layer == 2) ++nHitsL2;
      else if (layer == 3) ++nHitsL3;
    }
  }
  // decorate the jet with something
  m_nHits_L0(jet) = nHitsL0;
  m_nHits_L1(jet) = nHitsL1;
  m_nHits_L2(jet) = nHitsL2;
  m_nHits_L3(jet) = nHitsL3;
  
}
