#include "TrackAmbiDecorator.hh"

// the constructor just builds the decorator
TrackAmbiDecorator::TrackAmbiDecorator(const std::string& prefix):
  m_ambiRank(prefix + "ambiRank")
{
}

// this call actually does the work on the jet
void TrackAmbiDecorator::decorateAll(const xAOD::TrackParticleContainer& tracks) const {

  float n_tracks = tracks.size();

  for ( unsigned int i = 0; i < tracks.size(); i++ ) {
    auto track = tracks.at(i);
    m_ambiRank(*track) = float(i) / n_tracks;
  }
}
