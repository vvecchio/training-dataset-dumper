#ifndef TRACK_DECORATOR_EXAMPLE_HH
#define TRACK_DECORATOR_EXAMPLE_HH

#include "xAODTracking/TrackParticle.h"

#include <string>

// this is a bare-bones example of a class that could be used to
// decorate a track with additional information

class TrackDecoratorExample
{
public:
  TrackDecoratorExample(const std::string& decorator_prefix = "example_");

  // this is the function that actually does the decoration
  void decorate(const xAOD::TrackParticle& track) const;
private:
  // All this class does is apply a decoration, so all it needs to do
  // is contain one decorator. We could have also written a function
  // and statically initalized this, but static data in functions is
  // probably best avoided.
  SG::AuxElement::Decorator<float> m_deco;
};

#endif
