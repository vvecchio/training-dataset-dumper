#include "TrackDecoratorExample.hh"

// the constructor just builds the decorator
TrackDecoratorExample::TrackDecoratorExample(const std::string& prefix):
  m_deco(prefix + "decorator")
{
}

// this call actually does the work on the jet
void TrackDecoratorExample::decorate(const xAOD::TrackParticle& track) const {

  // Store something to the jet.
  m_deco(*track) = std::log(track.pt());
}
