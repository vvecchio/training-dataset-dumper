#!/usr/bin/env bash

# CONFIG
CONFIG=$1
CODEDIR=$(dirname $PWD/../training-dataset-dumper/configs/single-b-tag/TracklessEMPFlow.json/)/../../..

# Output directory
OUTDIR=$2

# Number of job
JOB=$3

# Input files
INPUTFILES=(${@:4})

# First copy the files to the tmp dir
DIR=$(mktemp -d)

echo "Running from directory: $DIR"

# Set up - and recompile because worker nodes apparently don't have the same gcc version...
set +e
cd $CODEDIR
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
. ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
asetup AnalysisBase,master,latest
cd build
cmake ../training-dataset-dumper
make
. x*/setup.sh

# Go back to running directory
cd $DIR

# Copy the input files here
for i in "${INPUTFILES[@]}" ; do
  cp $i .
done

echo "Done copying files"
ls
echo "------------------"

# Run executable
echo -e "\n\nWill now run: dump-single-btag -c $CONFIG $(ls *root* | tr '\n' ' ')\n\n"
dump-single-btag -c $CONFIG $(ls *root* | tr '\n' ' ')
echo -e "\n\nFinished run\n\n"

# Check if output exists and copy it to the input directory
if [ ! -f output.h5 ] ; then
  echo "ERROR: Output file not created"
  exit 1
else
  echo "Copying output file to $OUTDIR/output_$JOB.h5"
  cp output.h5 $OUTDIR/output_$JOB.h5  
fi 

exit 0
