#!/usr/bin/env bash

####################
# Helper functions #
####################

printError() {
  if [[ "$1" == "-f" ]] ; then
    local rest
    shift
    color="\033[1;31m"
    format=$1
    shift
    for arg in "$@" ; do rest+=" $arg" ; done
    printf "$color$format\033[0m\n" $rest
  else
    printf "\033[1;31m%s\033[0m\n" "$1"
  fi
}

printWarning() {
  if [[ "$1" == "-f" ]] ; then
    local rest
    shift
    color="\033[1;33m"
    format=$1
    shift
    for arg in "$@" ; do rest+=" $arg" ; done
    printf "$color$format\033[0m\n" $rest
  else
    printf "\033[1;33m%s\033[0m\n" "$1"
  fi
}

printGood() {
  if [[ "$1" == "-f" ]] ; then
    local rest
    shift
    color="\033[1;32m"
    format=$1
    shift
    for arg in "$@" ; do rest+=" $arg" ; done
    printf "$color$format\033[0m\n" $rest
  else
    printf "\033[1;32m%s\033[0m\n" "$1"
  fi
}

printInfo() {
  if [[ "$1" == "-f" ]] ; then
    local rest
    shift
    color="\033[1;34m"
    format=$1
    shift
    for arg in "$@" ; do rest+=" $arg" ; done
    printf "$color$format\033[0m\n" $rest
  else
    printf "\033[1;34m%s\033[0m\n" "$1"
  fi
}

# Usage
printHelp() {
  printInfo -f "\nUsage:"
  printInfo -f "batch-single-btag -c CONFIG -i INPUT [-o OUTPUT -h]\n"
  echo -e "CONFIG   \t is the path to the config file\n"
  echo -e "INPUT    \t is the path to the input files\n"
  echo -e "OUTPUT   \t is the path where the output files will be stored (default is PWD/batch_output)\n"
  echo -e "NFILES   \t number of files to submit in each job (default is 100)\n"
  echo -e "-t	    \t only submit a single job for testing\n"
  echo -e "-h|--help\t prints this help message\n"
}

# Function to create submission file template
createSubmissionFile() {
  SUBMITDIR=$PWD/submit
  
  if [ ! -d $SUBMITDIR ] ; then
    mkdir $SUBMITDIR
  fi
  
  SUBTEMPLATE=$SUBMITDIR/sub
  if [ -f $SUBTEMPLATE ] ; then rm -f $SUBTEMPLATE ; fi
  
  cat << EOF >> $SUBTEMPLATE
  Notification  = Never

  Executable    = $Dumper_DIR/../../training-dataset-dumper/BTagTrainingPreprocessing/batch/run.sh
  Arguments     = CONFIG OUTDIR JOB INPUTFILES

  GetEnv        = False

  Output        = SUBMITDIR/JOB.out
  Error         = SUBMITDIR/JOB.err

  #+RequestRuntime = 7000

  Queue 
EOF
}

# Function to get input files
getInputFiles() {
  inputFiles=($(ls $INPUTDIR/*pool.root* | tr '\n' ' '))
}

# Function to submit jobs to condor
submit() {
  printInfo "Getting input files"
  getInputFiles
  if $TEST ; then
    REMAINDER=0
    NJOBS=1
  else
    REMAINDER=$(( ${#inputFiles[@]} % $NFILESPERJOB ))
    NJOBS=$(( ${#inputFiles[@]} / $NFILESPERJOB ))
    if (( $REMAINDER > 0 )) ; then let NJOBS=$NJOBS+1 ; fi
  fi
  # Print summary
  printGood -f "\tFound: ${#inputFiles[@]} input files - will submit: $NJOBS jobs"
  
  # Loop over jobs
  for i in `seq 1 $NJOBS` ; do
    filesToRun=()
    if (( $i == $NJOBS && $REMAINDER != 0 )) ; then
      firstFile=$(( ($i-1)*$NFILESPERJOB ))
      lastFile=$(( ($i-1)*$NFILESPERJOB+$REMAINDER ))      
    else
      firstFile=$(( ($i-1)*$NFILESPERJOB ))
      lastFile=$(( $i*$NFILESPERJOB - 1 ))
    fi
    
    # Populate input file list
    for f in `seq $firstFile $lastFile` ; do
      filesToRun+=(${inputFiles[$f]})
    done
    
    # Prepare submission file
    sed "s|SUBMITDIR|$SUBMITDIR|g" $SUBTEMPLATE > $SUBMITDIR/sub_$i
    sed -i "s|CONFIG|$CONFIG|g" $SUBMITDIR/sub_$i
    sed -i "s|OUTDIR|$OUTPUTDIR|g" $SUBMITDIR/sub_$i
    sed -i "s|JOB|$i|g" $SUBMITDIR/sub_$i
    sed -i "s|INPUTFILES|${filesToRun[*]}|g" $SUBMITDIR/sub_$i
    
    # Submit the job
    condor_submit $SUBMITDIR/sub_$i
    
    # Clean up
    
  done
}


####################
#     Checks       #
####################

# Verify that the script is not sourced
if [[ $- == *i* ]] ; then
  printError "Don't source me!"
  return 1
fi

# Find batch system 
if command -v condor_submit >&/dev/null ; then
  BATCH=condor
elif command -v qsub >&/dev/null ; then
  BATCH=qsub
elif command -v bsub >&/dev/null ; then
  BATCH=bsub
fi

# Check whether we are running on condor
if [[ "$BATCH" != "condor" ]] ; then
  printError "Only condor implemented at the moment"
  exit 2
fi


# Get command-line arguments
if [[ "$#" < 1 ]] ; then
  printError "ERROR: no arguments specified"
  printHelp
  exit 2
fi

# defaults
TEST=false
NFILESPERJOB=20
OUTPUTDIR=$PWD

while [[ $# -gt 0 ]] ; do
  case $1 in
    -h|--help)
      printHelp
      exit 0
      ;;
    -c)
      CONFIG=$2
      shift
      ;;
    -i)
      INPUTDIR=$2
      shift
      ;;
    -o)
      OUTPUTDIR=$2
      shift
      ;;
    -n)
      NFILESPERJOB=$2
      shift
      ;;  
    -t)
      TEST=true
      ;;  
    *)
      printError "ERROR: Unrecognised option $1"
      printHelp
      exit 3
      ;;
  esac
  shift
done

# Check that the input directory exists
if [ -z $INPUTDIR ] ; then
  printError "ERROR: you have to provide an input directory with the -i option"
  exit 4
fi
if [ ! -d $INPUTDIR ] ; then
  printError "ERROR: input directory $INPUTDIR does not exist"
  exit 5
fi

# Check if config file exists
if [ ! -f $CONFIG ] ; then
  printError "ERROR: config file $CONFIG does not exist"
  exit 6
fi

# If OUTPUTDIR does not exist, create it
if [ ! -d $OUTPUTDIR/batch_output ] ; then
  mkdir $OUTPUTDIR/batch_output
  OUTPUTDIR=$OUTPUTDIR/batch_output
else 
  printError "Output directory $OUTPUTDIR/batch_output already exists. You should give a new directory name"
  exit 7
fi


####################
#   Submission     #
####################

# Create submission file template
createSubmissionFile

# Submit jobs
submit

printGood "All done"

exit 0
